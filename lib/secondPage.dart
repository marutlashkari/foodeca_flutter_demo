import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'finalPage.dart';
import 'notification.dart';
import 'package:http/http.dart' as http;
import 'apiPage.dart';
import 'dart:convert';


class Myorder extends StatefulWidget {
  const Myorder({Key? key}) : super(key: key);

  @override
  State<Myorder> createState() => _MyorderState();
}

class _MyorderState extends State<Myorder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Api Demo"),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return InsertUser(map: null);
                  },
                ),
              ).then(
                    (value) {
                  setState(() {});
                },
              );
            },
            child: Icon(Icons.add),
          ),
        ],
      ),
      body: Container(
        child: FutureBuilder<http.Response>(
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              dynamic jsonData = jsonDecode(snapshot.data!.body.toString());
              return ListView.builder(
                itemCount: jsonData.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return InsertUser(
                              map: jsonData[index],
                            );
                          },
                        ),
                      ).then(
                            (value) {
                          setState(() {});
                        },
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: ListTile(
                        tileColor: Colors.grey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        title: Text(jsonData[index]['Foodid']),
                        subtitle: Column(
                          children: [
                            Text(jsonData[index]['Foodprice']),
                            Text(jsonData[index]['Foodname'])
                          ],
                        ),

                        leading: CircleAvatar(
                          backgroundImage:
                          NetworkImage(jsonData[index]['FoodImg']),
                        ),
                        trailing: InkWell(
                          onTap: () {
                            deleteUser(jsonData[index]["Foodid"]).then((value) {
                              setState(() {
                              });
                            },);
                          },
                          child: Icon(Icons.delete),
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
          future: getPrinterData(),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home_filled),
                  label: '',
                  backgroundColor: Colors.black),
              BottomNavigationBarItem(
                icon: Icon(Icons.backpack),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add_alert_rounded),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings_rounded),
                label: '',
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future<http.Response> getPrinterData() async {
    print("Step 1");
    http.Response res = await http
        .get(Uri.parse('https://630da65f109c16b9abea3faf.mockapi.io/data'));
    print(res.body.toString());
    return res;
  }

  Future<void> deleteUser(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://630da65f109c16b9abea3faf.mockapi.io/data/$id'));
  }
}

