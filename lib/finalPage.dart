import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'secondPage.dart';
import 'package:foodeca_demo/secondPage.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({Key? key}) : super(key: key);

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(57, 62, 70, 0.7),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(109, 152, 134, 0.7),
        title: Container(
          margin: EdgeInsets.only(top: 4),
          child: Center(
            child: Text("My order",
                style: TextStyle(color: Colors.white, fontSize: 25)),
          ),
        ),
        leading: InkWell(
          onTap: () {
            // Navigator.pop(context);
            Navigator.of(context).pop(context);
          },
          child: Icon(Icons.arrow_back_ios_new),
        ),
        actions: [
          Icon(Icons.more_vert),
        ],
        // backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 50, left: 50, top: 10),
            child: Container(
              padding: EdgeInsets.all(20),
              // width: 200,
              // height: 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(26),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Pizza",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 30)),
                      CircleAvatar(
                        backgroundColor: Colors.grey.withOpacity(0.5),
                        child:
                            Text("1/1", style: TextStyle(color: Colors.black)),
                      )
                    ],
                  ),
                  Text("240gm - \$20", style: TextStyle(fontSize: 20)),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Container(
                      width: double.infinity,
                      height: 200,
                      color: Colors.white,
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2012/04/01/16/51/pizza-23477__340.png',
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    margin: EdgeInsets.only(top: 12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.grey,
                    ),
                    child: Center(
                      child: Text(
                        " Pay Now - \$20",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Details",
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                Text("See more"),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        width: 220,
                        height: 200,
                        padding: EdgeInsets.only(left: 4),
                        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.white,
                        ),
                        child: Text("helloa"),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        width: 220,
                        height: 200,
                        padding: EdgeInsets.only(left: 4),
                        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.white,
                        ),
                        child: Text("hello"),
                      ),
                    ),
                    Expanded(child: Container(
                      width: 220,
                      height: 200,
                      padding: EdgeInsets.only(left: 4, top: 20),
                      margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                      ),
                      child: ElevatedButton(
                        child: Container(
                          height: 100,
                          width: 100,
                          child: Text(
                            ' Pay Now - \$20',
                          ),
                        ),
                        onPressed: () {
                          MaterialPageRoute(builder: (context) {
                            return OrderPage();
                          },);
                        },
                      ),
                      // child: Icon(Icons.safety_check),alignment: Alignment.topCenter,
                    ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
