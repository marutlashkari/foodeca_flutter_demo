import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodeca_demo/secondPage.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
              child: Container(
            child: Text(
              'FOODECA',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'Merienda'
              ),
            ),
            margin: EdgeInsets.only(bottom: 570),
          )),
          Container(
              child: Expanded(
                  child: Container(
            child: Image.asset("images/Foodbest-removebg-preview.png"),

            height: 480,
            width: 2000,
            margin: EdgeInsets.only(bottom: 50,top: 25),
          ))),
          Center(
            child: Container(
              child: Text(
                'Choose your preference',
                style: TextStyle(fontSize: 10, color: Colors.grey),
              ),
              margin: EdgeInsets.only(top: 340),
            ),
          ),
          Center(
            child: Container(
              child: Text("What's Your",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
              margin: EdgeInsets.only(top: 380),
            ),
          ),
          Center(
              child: Container(
                  margin: EdgeInsets.only(top: 430),
                  child: Text(
                    "favorite food?",
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold),
                  ))),
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 550),
              child: SizedBox(
                width: 200.0,
                height: 50.0,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.black,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40))),
                  child: Text('Get Started'),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => Myorder()));
                  } ,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
