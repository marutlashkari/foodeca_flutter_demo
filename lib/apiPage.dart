import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertUser extends StatefulWidget {
  InsertUser({Key? key , this.map}) : super(key: key);

  Map? map;

  @override
  State<InsertUser> createState() => _InsertUserState();
}

class _InsertUserState extends State<InsertUser> {
  var formKey = GlobalKey<FormState>();

  TextEditingController priceController = TextEditingController();

  TextEditingController nameController = TextEditingController();

  TextEditingController idController = TextEditingController();

  TextEditingController imageController = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    priceController.text = widget.map == null?'':widget.map!["Foodprice"];
    nameController.text = widget.map == null?'':widget.map!["Foodname"];
    idController.text = widget.map == null?'':widget.map!["Foodid"];
    imageController.text = widget.map == null?'':widget.map!["FoodImg"];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add User"),
      ),
      body: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                controller: priceController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter The Foodprice";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter The Foodname";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: idController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter The Foodid";
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: imageController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Enter The FoodImg";
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    isLoading = true;
                  });
                  if(formKey.currentState!.validate()){
                    if(widget.map == null){
                      insertUser().then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                    else{
                      updateUser(widget.map!['Foodid']).then((value) {
                        Navigator.of(context).pop(true);
                      },);
                    }
                  }
                },
                child: Container(
                  width: 180,
                  height: 50,
                  padding:
                  EdgeInsets.only(top: 10, right: 50, bottom: 10, left: 50),
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: isLoading?CircularProgressIndicator():Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> insertUser() async {

    Map map = {};
    map["Foodid"] = idController.text.toString();
    map["Foodprice"] = priceController.text.toString();
    map["FoodImg"] = imageController.text.toString();

    http.Response res = await http.post(Uri.parse('https://630da65f109c16b9abea3faf.mockapi.io/data'),body: map);
  }
  Future<void> updateUser(id) async {

    Map map = {};
    map["Foodid"] = idController.text.toString();
    map["Foodprice"] = priceController.text.toString();
    map["FoodImg"] = imageController.text.toString();

    http.Response res = await http.put(Uri.parse('https://630da65f109c16b9abea3faf.mockapi.io/data/$id'),body: map);
  }
}
